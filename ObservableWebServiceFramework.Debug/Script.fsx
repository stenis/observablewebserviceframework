﻿#r @"..\ObservableWebServiceFramework.DesignTime\bin\Debug\FsPickler.dll"
#r @"..\ObservableWebServiceFramework.DesignTime\bin\Debug\ObservableWebServiceFramework.dll"
#r @"packages\FSharp.Control.Reactive.3.2.0\lib\net40\FSharp.Control.Reactive.dll"

open FSharp.Control.Reactive

type Api = ObservableWebServiceFramework.Endpoint< "localhost" >

let sub = 
    Api.values
    |> Api.filter <@ fun i -> i.Value < 100L @>
    |> Api.take 10
    |> Api.toObservable
    |> Observable.subscribe (fun value -> printfn "Temperature: %d" value.Value)
