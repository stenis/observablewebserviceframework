﻿module Main

open ObservableWebServiceFramework
open ObservableWebServiceFramework.Network
open System
open Microsoft.FSharp.Reflection
open Microsoft.FSharp.Quotations
open FSharp.Quotations.Evaluator
open FSharp.Control.Reactive

type RemoteValue = {
    Value : int64
    Key : string
    TimeStamp : DateTime
}

type Service() =
    member __.values 
        with get () = 
            Observable.interval
            <| (TimeSpan.FromSeconds 1.0)
            |> Observable.map (fun i -> { Value = i; Key = "Value"; TimeStamp = DateTime.Now })
            
    member __.take (count : int) (obs : IObservable<RemoteValue>) = 
        obs
        |> Observable.take count
    
    member __.filter (filterExpr : Expr<RemoteValue -> bool>) (obs : IObservable<RemoteValue>) = 
        printfn "%A" filterExpr 
        let filter = QuotationEvaluator.Evaluate filterExpr
        obs 
        |> Observable.filter filter

    member __.skip (count : int) (obs : IObservable<RemoteValue>) = 
        obs 
        |> Observable.skip count

let service = Service()
//let con = service 
//    |> ObservableWebServiceFramework.Contract.extractServiceDescription
//    |> (printfn "%A")

let disposable = ObservableWebService.Create(service, port = 8090)
Console.ReadLine() |> ignore
disposable.Dispose()