﻿namespace ObservableWebServiceFramework

open System
open System.IO
open System.Xml.Linq
open System.Reflection
open System.Reflection.Emit
open System.Reactive
open System.Reactive.Linq
open System.Text.RegularExpressions
open System.Collections.Generic
open Microsoft.FSharp.Core
open Microsoft.FSharp.Core.CompilerServices
open Microsoft.FSharp.Reflection
open Microsoft.FSharp.Quotations
open ProviderImplementation.ProvidedTypes
open ObservableWebServiceFramework
open ObservableWebServiceFramework.Network
open ObservableWebServiceFramework.TypeParser
open Nessos.FsPickler
#nowarn "25"

[<AutoOpen>]
module internal ProvidedTypeExtensions = 
     
    let xname = XName.Get
    
    let creatTypeResolver (customTypes : IDictionary<string, ProvidedTypeDefinition>)=
        let getType name = if customTypes.ContainsKey name then customTypes.[name] :> Type else Type.GetType(name) 
        let rec changeType (Type'(name, args)) =
            match args with
            | [] -> getType name
            | list -> ProvidedTypeBuilder.MakeGenericType(getType(name), list |> List.map changeType)

        fun (typename : string) ->
            parse parseArgumentTypes &(sprintf "[%s]" typename)
            |> function
            | (root, _)::t -> changeType root
            | _ -> failwith <| sprintf "Error parsing: %s" typename

    
    let extractCustomTypes (doc : XDocument) =
        doc.Descendants(xname "CustomType")
        |> Seq.map (fun c -> 
            let name = c.Attribute(xname "Name").Value
            let assemblyQualifiedName = c.Attribute(xname "AssemblyQualifiedName").Value
            let properties = 
                c.Elements(xname "Property")
                |> Seq.collect (fun p ->
                    let name = p.Attribute(xname "Name").Value
                    let ty = p.Attribute(xname "Type").Value |> Type.GetType
                    let field = ProvidedField("_" + name.ToLower(), ty)
                    let prop = ProvidedProperty(name, ty, 
                                GetterCode=(fun [this] -> Expr.FieldGet(this, field)),
                                SetterCode=(fun [this;value] -> Expr.FieldSet(this, field, value)))
                    [field :> MemberInfo; prop :> MemberInfo])
                |> Seq.toList

            let customTy = ProvidedTypeDefinition(name, Some typeof<obj>, IsErased = false)
            let ctor = ProvidedConstructor([], InvokeCode=(fun _ -> <@@ () @@>)) :> MemberInfo
            let saTy = typeof<SerializableAttribute>
            let saCtor = saTy.GetConstructor([||])
            { new CustomAttributeData() with 
                member __.Constructor = saCtor
                member __.ConstructorArguments = upcast [| |]
                member __.NamedArguments = upcast [| |] } 
            |> customTy.AddCustomAttribute

            customTy.AddMembers (ctor::properties) 
            assemblyQualifiedName, customTy)
        |> dict
    
    let private createLambdaType types = ProvidedTypeBuilder.MakeGenericType(typedefof<_ -> _>, types)
    
    let f (pi : ParameterInfo) = 
        let pt = pi.ParameterType
        if pt.IsGenericType && pt.GetGenericTypeDefinition() = typedefof<_ -> _> then
            typedefof<Expr<_>>.MakeGenericType(pi.ParameterType)
        else    
            pt
    
    let extractMethods typeResolver (converter : FieldInfo) (doc : XDocument) =
        doc.Descendants(xname "Method")
        |> Seq.mapi (fun i m -> 
            let name = m.Attribute(xname "Name").Value
            let returnType = typedefof<Subscription>
            let parameters = 
                m.Descendants(xname "Parameter")
                |> Seq.map (fun p ->
                    let name = p.Attribute(xname "Name").Value
                    let typename = p.Attribute(xname "Type").Value
                    let ty = typeResolver typename
                    name, ty)
                |> Seq.toList

            let providedParameters, invokeCode, ret = 
                match parameters with
                | [] -> [], (fun args -> <@@ null @@>), returnType
                | (name, ty)::[] -> [ProvidedParameter(name, ty)], (fun (args : Expr list) -> args.[0]), returnType
                | (n1, t1)::_::[] -> 
                    let lambdaTy = createLambdaType [typedefof<Subscription>; typedefof<Subscription>] 
                    [ProvidedParameter(n1, t1)],
                    (fun (args : Expr list) ->
                        let serializeSubscriptionMethod = ProvidedTypeBuilder.MakeGenericMethod(typedefof<Runtime>.GetMethod("serializeSubscription"), [t1])
                        Expr.Call(serializeSubscriptionMethod, [Expr.Value(name); Expr.Value(i); Expr.FieldGet(converter); args.[0]])),
                        lambdaTy
                | (name, ty)::t -> 
                    //TODO: Fix
                    failwith "not supported at the moment"

            ProvidedMethod(name, providedParameters, ret, IsStaticMethod = true, InvokeCode = invokeCode))
        |> Seq.toList

    //TODO: Refactor this to check all methods returning Obserable<'T>, we need to add a toObseravable for all these aswell
    let extractObservables typeResolver hostname port (doc : XDocument) =
        doc.Descendants(xname "Observable")
        |> Seq.mapi (fun i p -> 
            let name = p.Attribute(xname "Name").Value
            let fullyQualifiedTypename = p.Attribute(xname "Type").Value
            let typename = fullyQualifiedTypename.Substring(0, fullyQualifiedTypename.IndexOf(','))
            let ty = typeResolver fullyQualifiedTypename
            let obsTy = typedefof<IObservable<_>>.MakeGenericType([| ty |])
            let createObservableMethod = 
                ProvidedTypeBuilder.MakeGenericMethod((typedefof<Runtime>).GetMethod("createClientSideObservable"), [ty])

            let obsProperty = 
                ProvidedProperty(
                    name,
                    typedefof<Subscription>, 
                    IsStatic = true, 
                    GetterCode=(fun args -> <@@ Subscription([Endpoint(name, BitConverter.GetBytes(i))])@@>))

            let toObservable =
                ProvidedMethod(
                    methodName = "toObservable", 
                    parameters = [ProvidedParameter("source", typedefof<Subscription>)],
                    returnType = obsTy,
                    IsStaticMethod = true,
                    InvokeCode = 
                        (fun args ->
                            Expr.Sequential(
                                <@@ (%%(args.[0]) : Subscription).print() @@>, 
                                Expr.Call(createObservableMethod, [Expr.Value(typename);Expr.Value(hostname); Expr.Value(port); args.[0]]))))
            
            [toObservable :> MemberInfo; obsProperty :> MemberInfo])
        |> Seq.concat
        |> Seq.toList
    
    let createTypeNameConverter namespace' (doc : XDocument) =
        let mapping = 
            doc.Descendants(XName.Get "CustomType")
            |> Seq.map (fun c -> 
                let clientName = sprintf "%s+%s" namespace' (c.Attribute(xname "Name").Value)
                let serviceName = c.Attribute(xname "FullName").Value
                let serviceAssemblyName = c.Attribute(xname "AssemblyName").Value
                clientName, serviceName, serviceAssemblyName)
            |> Seq.toArray

        let converter = ProvidedTypeDefinition("TypeConverter", Some typedefof<obj>, IsErased = false)
        let tiTy = typedefof<Nessos.FsPickler.TypeInfo>
        let toDeserializedType = 
            ProvidedMethod(
                "ToDeserializedType", 
                [ProvidedParameter("ti", tiTy)], 
                tiTy, 
                InvokeCode = (fun args -> args.[1]))
        
        let ofSerializedType = 
            ProvidedMethod(
                "OfSerializedType", 
                [ProvidedParameter("ti", tiTy)], 
                tiTy,
                InvokeCode = (fun args -> 
                    if mapping.Length > 0 then 
                        Array.fold (fun state (clientName, serviceName, serviceAssemblyName) -> 
                            Expr.IfThenElse(
                                <@@ (%%args.[1] : Nessos.FsPickler.TypeInfo).Name = clientName @@>, 
                                <@@ Runtime.createTypeInfo(serviceName, serviceAssemblyName) @@>, 
                                state))
                            args.[1] 
                            mapping
                    else
                        args.[1]))

        let attrs = MethodAttributes.Public ||| MethodAttributes.Virtual
        toDeserializedType.AddMethodAttrs(attrs)
        ofSerializedType.AddMethodAttrs(attrs)

        let ctor = ProvidedConstructor([], InvokeCode = (fun _ -> <@@ () @@>))
        converter.AddInterfaceImplementation(typedefof<ITypeNameConverter>)
        converter.AddMembers [toDeserializedType; ofSerializedType]
        converter.AddMember ctor
        converter, ctor

[<TypeProvider>]
type public ApiBuilderProvider(cfg: TypeProviderConfig) as this =
    inherit TypeProviderForNamespaces()
    let asm = Assembly.GetExecutingAssembly()
    let ns = this.GetType().Namespace
    let pn = "Endpoint"
    let staticParams = [
        ProvidedStaticParameter("url", typeof<string>)
        ProvidedStaticParameter("port", typeof<int>, 8090)]

    do 
        AppDomain.CurrentDomain.add_AssemblyResolve (fun _ args ->
            let localAssemblies = AppDomain.CurrentDomain.GetAssemblies()
            let referencedAssemblies = cfg.ReferencedAssemblies
            let name = AssemblyName(args.Name)
            let matchesDefinition(n1, n2) = AssemblyName.ReferenceMatchesDefinition(n1, n2)
            
            localAssemblies
            |> Seq.tryFind(fun a -> matchesDefinition(name, a.GetName()))
            |> function
            | Some a -> a
            | None -> 
                referencedAssemblies 
                |> Seq.tryFind (fun a -> matchesDefinition(AssemblyName.GetAssemblyName(a), name))
                |> function
                | Some a -> Assembly.LoadFrom a
                | None -> null)

        let builderTy = ProvidedTypeDefinition(asm, ns, pn, Some typeof<obj>, IsErased = false)
        builderTy.DefineStaticParameters(staticParams, this.instantiationFunction)
        this.AddNamespace(ns, [builderTy])

    member internal this.instantiationFunction typeName (parameterValues : obj[]) =
        match parameterValues with 
            | [| :? string as hostname; :? int as port |] ->
                let contract = getContract hostname port
                let tempAsm = ProvidedAssembly(Path.ChangeExtension(Path.GetTempFileName(), ".dll"))
                let containerTy = ProvidedTypeDefinition(asm, ns, typeName, Some typeof<obj>, IsErased = false)
                let customTypes = contract |> extractCustomTypes 
                let typeResolver = creatTypeResolver customTypes
                let converterTy, converterTyCtor = createTypeNameConverter (sprintf "%s.%s" ns typeName) contract
                let converterField = ProvidedField("converter", typedefof<ITypeNameConverter>)
                converterField.SetFieldAttributes(FieldAttributes.Static ||| FieldAttributes.Private)

                customTypes.Values |> Seq.iter containerTy.AddMember
                extractObservables typeResolver hostname port contract |> containerTy.AddMembers
                contract |> extractMethods typeResolver converterField |> containerTy.AddMembers
                              
                let ctor = 
                    ProvidedConstructor(
                        [], 
                        IsTypeInitializer = true, 
                        InvokeCode = (fun args -> 
                            Expr.FieldSet(converterField, Expr.Coerce(Expr.NewObject(converterTyCtor, []), typedefof<ITypeNameConverter>))))
                
                containerTy.AddMember converterTy
                containerTy.AddMember converterField
                containerTy.AddMember ctor
                tempAsm.AddTypes [containerTy]
                containerTy

            | _ -> failwith "unexpected parameter values"
