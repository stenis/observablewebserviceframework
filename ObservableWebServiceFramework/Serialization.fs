﻿module Serialization

open System
open Microsoft.FSharp.Core
open Microsoft.FSharp.Quotations

// Define your library scripting code here


type Endpoint<'a> =
    | Endpoint of 'a

type XBuilder() =
//    member __.Return x = Some x
    member __.Bind (m : 'a option, f : 'a -> 'a option) = match m with Some x -> f x | _ -> None
    member __.Yield v = Some v
    member __.For(m, f) = 
        __.Bind(m, f)
    member __.Zero() = Some 0

    [<CustomOperation("value", MaintainsVariableSpace=false)>]
    member __.Value(_) = 
        __.Zero() 

    [<CustomOperation("add", MaintainsVariableSpace=true)>]
    member __.Add(v, (one : int)) = 
        match v with
        | Some x -> 
            let v = x + one 
            printfn "%d" v
            Some v
        | _ -> None 
    //member __.Quote() = ()
    //member __.Run(q) = q


let x = XBuilder()

let y = x {
    value
    add 10
    add 30
}