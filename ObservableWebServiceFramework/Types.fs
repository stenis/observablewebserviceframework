﻿namespace ObservableWebServiceFramework

open System

type MessageType =
    | OnNext
    | OnCompleted
    | OnError
    | RequestContract
    | Subscribe
    with 
        static member fromInt i =
            match i with
            | 0 -> OnNext
            | 1 -> OnCompleted
            | 2 -> OnError
            | 3 -> RequestContract
            | 4 -> Subscribe
            | _ -> failwith <| sprintf "%d is not a vaild index for MessageType." i

        member this.Bytes 
            with get() = 
                match this with
                | OnNext -> 0
                | OnCompleted -> 1
                | OnError -> 2
                | RequestContract -> 3
                | Subscribe -> 4
                |> BitConverter.GetBytes

type Pipe = 
    | Endpoint of string * byte array
    | Method of (string * byte array) 
  
type Subscription = Subscription of Pipe list
    with
        member this.toArray() =
            this 
            |> function Subscription list -> list 
            |> List.rev
            |> List.map (function
                | Endpoint(_, b) -> b
                | Method(_, b) -> b)
            |> List.toArray
            |> Array.concat

        member this.print() =
            this 
            |> function Subscription list -> list 
            |> List.rev
            |> List.iter (function
                | Endpoint(name, _) -> printfn "Endpoint %s" name
                | Method(name, _) -> printfn "Method %s" name) 