﻿namespace ObservableWebServiceFramework

open System
open System.IO
open System.Net.Sockets
open System.Reflection
open System.Runtime.Serialization
open System.Text
open System.Xml.Linq
open Nessos.FsPickler

module Contract =
    
    let private xname = XName.Get
    
    let private addAttribute name value (node : XElement) = 
        node.SetAttributeValue(xname name, value)
        node

    let private createElement elementName name = 
        XElement(xname elementName)
        |> addAttribute "Name" name
       
    let private addTypeAttribute typename = addAttribute "Type" typename
        
    let private createParameterElement (p : ParameterInfo)  =
        createElement "Parameter" p.Name |> addTypeAttribute p.ParameterType.AssemblyQualifiedName

    let private createMethodElement (m : MethodInfo) =
        let node = createElement "Method" m.Name |> addTypeAttribute m.ReturnType.AssemblyQualifiedName
        let parameters = XElement(xname "Parameters")
        m.GetParameters() 
        |> Array.map createParameterElement
        |> Array.iter parameters.Add
        node.Add parameters
        node

    let private createPropertyElement (p : PropertyInfo) = 
        createElement "Property" p.Name |> addTypeAttribute p.PropertyType.AssemblyQualifiedName

    let private createObservableElement (p : PropertyInfo) = 
        createElement "Observable" p.Name 
        |> addTypeAttribute (p.PropertyType.GetGenericArguments().[0].AssemblyQualifiedName)
        
    let private createCustomTypeElement (t : Type) =
        let node = 
            createElement "CustomType" t.Name 
            |> addAttribute "FullName" t.FullName
            |> addAttribute "AssemblyQualifiedName" t.AssemblyQualifiedName
            |> addAttribute "AssemblyName" (t.Assembly.GetName().Name)

        t.GetProperties() |> Array.map createPropertyElement |> Array.iter node.Add
        node

    let private getCustomTypes (properies : PropertyInfo array) = 
        properies 
        |> Array.collect (fun p -> p.PropertyType.GetGenericArguments())
        |> Array.filter (fun t -> 
            t.Namespace = null || 
            not (t.Namespace = "System" || 
                 t.Namespace.StartsWith "Microsoft.FSharp.Core"))
    
    let private populateElement name (mapper : 'a -> XElement) elements = 
        XElement(xname name, elements |> Array.map mapper)
   
    let extractServiceDescription service = 
        let serviceType = service.GetType()
        let flags = BindingFlags.DeclaredOnly ||| BindingFlags.Public ||| BindingFlags.Instance
        let methods = serviceType.GetMethods(flags) |> Array.filter (fun m -> not m.IsSpecialName)
        let properties = serviceType.GetProperties()
        let customTypes = getCustomTypes properties
        XElement(xname "Contract",
            [| populateElement "Methods" createMethodElement methods
               populateElement "Observables" createObservableElement properties
               populateElement "CustomTypes" createCustomTypeElement customTypes |])
