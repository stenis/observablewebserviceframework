﻿namespace ObservableWebServiceFramework

open System
open System.Diagnostics
open System.IO
open System.Net
open System.Net.Sockets
open System.Threading
open System.Text

// From: https://gist.github.com/panesofglass/765088

module Network =
    
    open System.Xml.Linq

    type Socket with
        member socket.AsyncAccept() = Async.FromBeginEnd(socket.BeginAccept, socket.EndAccept)
 
    let trace : string -> unit = Console.WriteLine

    let close (stream : Stream) (socket : Socket) = 
        stream.Close()
        socket.Shutdown(SocketShutdown.Both)
        socket.Close()

    let createMessage (mt : MessageType) =
        let messageBytes = Array.zeroCreate<byte>(8)
        mt.Bytes.CopyTo(messageBytes, 0)
        messageBytes

    let createValueMessage (mt : MessageType) (bytes : byte array) =
        let messageBytes = Array.zeroCreate<byte>(8 + bytes.Length)
        mt.Bytes.CopyTo(messageBytes, 0)
        BitConverter.GetBytes(bytes.Length).CopyTo(messageBytes, 4)
        bytes.CopyTo(messageBytes, 8)
        messageBytes

    let readHeaderAsync (stream : NetworkStream) = async {
        let header = Array.zeroCreate<byte>(8)
        let! _ = stream.AsyncRead(header, 0, 8)
        let methodIndex = BitConverter.ToInt32(header, 0)
        let size = BitConverter.ToInt32(header, 4)
        return methodIndex, size
    }
           
    let getContract hostname port =
        try
            use client = new TcpClient(hostname, port)
            use stream = client.GetStream()
            async {
                let message = createMessage RequestContract
                do! stream.AsyncWrite message
                let! mi, size = readHeaderAsync stream
                let! bytes = stream.AsyncRead(size)
                return bytes
            } 
            |> Async.RunSynchronously
            |> Encoding.UTF8.GetString
        with
        | _ ->
            @"<Contract>
                  <Methods>
                    <Method Name=""take"" Type=""System.IObservable`1[[Main+Record, TypeBuilderProvider.Debug, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null]], mscorlib, Version=4.0.0.0, Culture=neutral, PublicKeyToken=b77a5c561934e089"">
                      <Parameters>
                        <Parameter Name=""count"" Type=""System.Int32, mscorlib, Version=4.0.0.0, Culture=neutral, PublicKeyToken=b77a5c561934e089"" />
                        <Parameter Name=""obs"" Type=""System.IObservable`1[[Main+Record, TypeBuilderProvider.Debug, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null]], mscorlib, Version=4.0.0.0, Culture=neutral, PublicKeyToken=b77a5c561934e089"" />
                      </Parameters>
                    </Method>
                    <Method Name=""filter"" Type=""System.IObservable`1[[Main+Record, TypeBuilderProvider.Debug, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null]], mscorlib, Version=4.0.0.0, Culture=neutral, PublicKeyToken=b77a5c561934e089"">
                      <Parameters>
                        <Parameter Name=""filterExpr"" Type=""Microsoft.FSharp.Quotations.FSharpExpr`1[[Microsoft.FSharp.Core.FSharpFunc`2[[Main+Record, TypeBuilderProvider.Debug, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null],[System.Boolean, mscorlib, Version=4.0.0.0, Culture=neutral, PublicKeyToken=b77a5c561934e089]], FSharp.Core, Version=4.3.1.0, Culture=neutral, PublicKeyToken=b03f5f7f11d50a3a]], FSharp.Core, Version=4.3.1.0, Culture=neutral, PublicKeyToken=b03f5f7f11d50a3a"" />
                        <Parameter Name=""obs"" Type=""System.IObservable`1[[Main+Record, TypeBuilderProvider.Debug, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null]], mscorlib, Version=4.0.0.0, Culture=neutral, PublicKeyToken=b77a5c561934e089"" />
                      </Parameters>
                    </Method>
                    <Method Name=""skip"" Type=""System.IObservable`1[[Main+Record, TypeBuilderProvider.Debug, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null]], mscorlib, Version=4.0.0.0, Culture=neutral, PublicKeyToken=b77a5c561934e089"">
                      <Parameters>
                        <Parameter Name=""count"" Type=""System.Int32, mscorlib, Version=4.0.0.0, Culture=neutral, PublicKeyToken=b77a5c561934e089"" />
                        <Parameter Name=""obs"" Type=""System.IObservable`1[[Main+Record, TypeBuilderProvider.Debug, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null]], mscorlib, Version=4.0.0.0, Culture=neutral, PublicKeyToken=b77a5c561934e089"" />
                      </Parameters>
                    </Method>
                  </Methods>
                  <Observables>
                    <Observable Name=""values"" Type=""Main+Record, TypeBuilderProvider.Debug, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null"" />
                  </Observables>
                  <CustomTypes>
                    <CustomType Name=""Record"" FullName=""Main+Record"" AssemblyQualifiedName=""Main+Record, TypeBuilderProvider.Debug, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null"" AssemblyName=""TypeBuilderProvider.Debug"">
                      <Property Name=""Value"" Type=""System.Int64, mscorlib, Version=4.0.0.0, Culture=neutral, PublicKeyToken=b77a5c561934e089"" />
                      <Property Name=""Key"" Type=""System.String, mscorlib, Version=4.0.0.0, Culture=neutral, PublicKeyToken=b77a5c561934e089"" />
                    </CustomType>
                  </CustomTypes>
                </Contract>"
        |> XDocument.Parse
