﻿namespace ObservableWebServiceFramework

open System
open System.IO
open System.Net.Sockets
open System.Reactive
open System.Reactive.Linq
open System.Threading
open System.Runtime.Serialization
open System.Reflection
open System.Text
open System.Xml.Linq
open System.Collections.Generic
open Nessos.FsPickler
open Microsoft.FSharp.Quotations
open FSharp.Quotations
open Network
    
type Runtime =

    static member createTypeInfo(name, assembly) = { 
        Name = name //"Main+Record"
        AssemblyInfo = 
            { Name = assembly //"TypeBuilderProvider.Debug"
              Version = null
              Culture = null
              PublicKeyToken = null }
    }
   
    static member serializeSubscription<'T>(name, methodIndex : int, converter : ITypeNameConverter, value : 'T) =
        fun (Subscription(values)) ->
            let binary = FsPickler.CreateBinary(typeConverter = converter)
            let valueBytes = binary.Pickle(value)
            let bytes = 
                Array.concat 
                    [| BitConverter.GetBytes methodIndex
                       BitConverter.GetBytes valueBytes.Length 
                       valueBytes |]

            let m = Method(name, bytes)
            Subscription(m::values)

    static member createSendClientOnNextMethod<'T> (stream : NetworkStream) =
        let binary = FsPickler.CreateBinary()
        Action<'T>(fun (value : 'T) ->
            //TODO: FIX THIS wtf stan, fix what? 
            createValueMessage OnNext <| binary.Pickle(value, StreamingContext(StreamingContextStates.All))
            |> stream.AsyncWrite
            |> Async.Start)

    static member createSendClientOnCompletedMethod<'T> (stream : NetworkStream) =
        let binary = FsPickler.CreateBinary()
        Action(fun () ->  
            createMessage OnCompleted
            |> stream.AsyncWrite
            |> Async.Start)

    static member getConverter (typename : string) = {
        new ITypeNameConverter with 
            member __.ToDeserializedType ti = ti
            member __.OfSerializedType (ti : Nessos.FsPickler.TypeInfo) = { 
                Name = typename
                AssemblyInfo = 
                    { Name = System.Reflection.Assembly.GetExecutingAssembly().FullName
                      Version = null
                      Culture = null
                      PublicKeyToken = null }
            }
        }

    static member createClientSideObservable<'T when 'T : (new : unit -> 'T)> (serverSideName, hostname, port, subscription : Subscription) =
        Observable.Create(fun (observer : IObserver<'T>) ->
            let ty = typeof<'T>
            let clientSideName = ty.Name
            let binary =
                if clientSideName = serverSideName then FsPickler.CreateBinary()
                else FsPickler.CreateBinary(typeConverter = Runtime.getConverter serverSideName)                
            
            let client = new TcpClient(hostname, port)
            let stream = client.GetStream()
            let cts = new CancellationTokenSource()
            
            let rec recieveLoop () = async {
                let! mi, size = readHeaderAsync stream
                let! bytes = stream.AsyncRead(size)
                match MessageType.fromInt mi with
                | OnCompleted -> observer.OnCompleted()
                | OnNext -> 
                    let value = binary.UnPickle(ty, bytes) :?> 'T
                    observer.OnNext value
                    return! recieveLoop()
                | _ -> 
                    //TODO: Add remote exception propagation / handling.
                    observer.OnError(Exception("Boom."))
            }

            let subscribeToEvents = async {
                let message = createValueMessage Subscribe <| subscription.toArray()
                do! stream.AsyncWrite message 
            }

            Async.Start(recieveLoop(), cts.Token)
            Async.Start(subscribeToEvents, cts.Token)

            Action(fun () ->
                cts.Cancel()
                stream.Close()
                client.Close()                
                observer.OnCompleted()))

    static member createSubscriptionHandler service =
        let serviceType = service.GetType()
        let flags = BindingFlags.DeclaredOnly ||| BindingFlags.Public ||| BindingFlags.Instance
        let observables = serviceType.GetProperties()
        let methods = 
            serviceType.GetMethods(flags) 
            |> Array.filter (fun m -> not m.IsSpecialName)
        
        let m = typedefof<Evaluator.QuotationEvaluator>.GetMethod("Evaluate")
        let f (pi : ParameterInfo) value : obj = 
            let pt = pi.ParameterType
            if pt.IsGenericType && pt.GetGenericTypeDefinition() = typedefof<Expr<_>> then
                m.MakeGenericMethod(pi.ParameterType).Invoke(null, [| value |])
            else    
                value

        fun (bytes : byte array) (stream : NetworkStream) ->
            use memoryStream = new MemoryStream(bytes)
            use reader = new BinaryReader(memoryStream)
            let binaryPickler = FsPickler.CreateBinary()
            let index = reader.ReadInt32()
            let observable = observables.[index]
            let obs = observable.GetGetMethod().Invoke(service, [||])            
            let rec createObservableChain (reader: BinaryReader) (obs : obj) =
                if reader.BaseStream.Position < reader.BaseStream.Length then
                    let method' = methods.[reader.ReadInt32()]
                    let pis = method'.GetParameters()
                    let params' = 
                        pis.[0 .. pis.Length-2] 
                        |> Array.map (fun pi -> 
                            let pickler = FsPickler.CreateBinary()
                            let size = reader.ReadInt32()
                            let bytes = reader.ReadBytes(size)
                            let value = pickler.UnPickle(pi.ParameterType, bytes)
                            
                            //f pi value
                            value
                        )
                    let obs' = method'.Invoke(service, Array.append params' [| obs |])
                    createObservableChain reader obs'
                else
                    obs
                
            let obs' = createObservableChain reader obs
            let subscribeMethodName = "System.IDisposable Subscribe[T](System.IObservable`1[T], System.Action`1[T], System.Action)"
            let subscribeMethod = 
                typeof<ObservableExtensions>.GetMethods()
                |> Array.find(fun t -> t.ToString() = subscribeMethodName)
            
            let args = obs'.GetType().GetGenericArguments()
            //TODO: properly handle 'T, now breaks on Interval.Timer that does not have an explicit generic argument (long)
            let ty = if obs' = obs then args.[0] else args.[args.Length-1]
            let util = typedefof<Runtime>
            let onNext = util.GetMethod("createSendClientOnNextMethod").MakeGenericMethod(ty).Invoke(null, [| stream |])
            let onCompleted = util.GetMethod("createSendClientOnCompletedMethod").MakeGenericMethod(ty).Invoke(null, [| stream |])            
            subscribeMethod.MakeGenericMethod(ty).Invoke(null, [| obs'; onNext; onCompleted |]) :?> IDisposable
