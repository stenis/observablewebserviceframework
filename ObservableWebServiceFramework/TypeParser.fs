﻿namespace ObservableWebServiceFramework

open System
open System.Collections.Generic

//From: http://www.codeproject.com/Articles/235221/Monadic-Parsing-in-Fsharp

module TypeParser =

    type Parser<'r> = Parser of (char list -> ('r*char list) list)

    let parse (Parser p) = p
    let (>>=) p f = Parser(fun cs -> List.concat [for (r,cs') in parse p cs -> parse (f r) cs'])
    let (>>) p q = p >>= fun _ -> q
    let mreturn r = Parser(fun cs -> [(r,cs)])
    let lambda = Parser(fun _ -> [])
    let item = Parser(fun cs -> match cs with [] -> [] | c::cs' -> [(c,cs')])
    let sat cond = item >>= fun c -> if cond c then mreturn c else lambda
    let char c = sat ((=)c)
      
    let (<|>) p q = Parser(fun cs ->
      match parse p cs with
      | [] -> parse q cs
      | rs -> rs)

    let rec many0 p = many1 p <|> mreturn []
        and many1 p = p >>= fun r -> many0 p >>= fun rs -> mreturn (r::rs)

    let sepBy p sep =
        p >>= fun r ->
        many0 (sep >> p) >>= fun rs ->
        mreturn (r::rs)

    let (~&) (str:string) = str.ToCharArray() |> List.ofArray
    let (~%) (chars:char list) = new String(Array.ofList chars)

    type Type' = Type' of string * Type' list

    let id = many1 (sat (fun c -> not (c = '[' || c = ']')))

    let rec parseArgumentTypes =
        ( char '[' >>
          id >>= fun name ->
          (char '[' >>
           sepBy parseArgumentTypes (char ',') >>= fun args ->
           char ']' >>
           id >>= fun rest ->
           let fqn = %name + %rest
           char ']' >>
           mreturn(Type'(fqn, args)))
           <|> (char ']' >> mreturn(Type'(%name, []))))
