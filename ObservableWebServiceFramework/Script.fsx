﻿// Learn more about F# at http://fsharp.net. See the 'F# Tutorial' project
// for more guidance on F# programming.

open System
open System.Net
open System.Text 
open System.Net.Sockets
open System.Text
open System.Threading

#r "bin\\Test\\System.Reactive.Interfaces.dll"
#r "bin\\Test\\System.Reactive.Core.dll"
#r "bin\\Test\\System.Reactive.Linq.dll"
#r "System.Xml.Linq.dll"

open System.Linq
open System
open System.Reflection
open System.Reactive
open System.Reactive.Linq

(typedefof<Observable>).GetMethods()
|> Array.map (fun i -> i.Name)
|> Set.ofArray
|> Seq.iteri (printfn "%d %s")

type Hello = { mutable Key : string; mutable Value : int64}

type Service() =
    member __.Values with get () = Observable.Interval(TimeSpan.FromSeconds 1.0)
    member __.As with get () = Observable.Interval(TimeSpan.FromSeconds 1.0).Select(fun i -> { Key = "Ciao"; Value = i})
    member __.Take (count : int) (obs : IObservable<Hello>) = obs.Take(count)
    member __.Skip (count : int) (obs : IObservable<Hello>)= obs.Skip(count)

let service = Service()
let contract = Contract.extract service


let inline pr (v : ^a array)= Array.iter (printfn "%A") v

#r "bin\\Debug\\FsPickler.dll"

open System.Text
open System.Runtime.Serialization
open Nessos.FsPickler

let bsType = typedefof<BinarySerializer>
let x = FsPickler.CreateBinary()
let v = x.Pickle 10


bsType.GetMethods() |> pr
let p = bsType.GetMethods() |> Array.find (fun m -> m.ToString() = "Byte[] Pickle[T](T, Microsoft.FSharp.Core.FSharpOption`1[System.Runtime.Serialization.StreamingContext], Microsoft.FSharp.Core.FSharpOption`1[System.Text.Encoding])")

let up = bsType.GetMethod("UnPickle", [| typeof<byte array>; typeof<Option<StreamingContext>>; typeof<Option<Encoding>> |])
 
let prs : obj array = [|
    v;
    Option<StreamingContext>.None;
    Option<Encoding>.None |]

let res =
    try
        Some (up.MakeGenericMethod([|typeof<int>|]).Invoke(x, prs))
    with
       | :? System.Reflection.TargetParameterCountException as e -> printfn "%A" (e); None


//#r "bin\\Debug\\FsPickler.dll"
//#r "bin\\Debug\\Newtonsoft.Json.dll"
//#r "bin\\Debug\\FsPickler.Json.dll"

open Nessos.FsPickler
open Nessos.FsPickler.Json

let json = FsPickler.CreateJson(indent = true)
let bson = FsPickler.CreateBson()

module X1 = 
    type R = { Value : int}
    let Rn = typeof<R>.ToString()

module X2 = 
    type R = { Value : int}
    let Rn = typeof<R>.ToString()

open X1
open Microsoft.FSharp.Quotations
open System.Runtime.Serialization

let s = StreamingContext(StreamingContextStates.All)
let v = json.PickleToString(<@ {Value = 10} @>, s).Replace(X1.Rn, X2.Rn)

printfn "%s" v

let f = json.UnPickleOfString<Expr<X2.R>>(v, s)

#r "bin\\Test\\System.Reactive.Interfaces.dll"
#r "bin\\Test\\System.Reactive.Core.dll"
#r "bin\\Test\\System.Reactive.Linq.dll"
#r "System.Xml.Linq.dll"

open System.Xml.Linq

let getContract hostname port =
    use client = new TcpClient(hostname, port)
    use stream = client.GetStream()
    async {
        let message = createMessage 1
        do! stream.AsyncWrite message
        let! mi, size = readHeaderAsync stream
        let! bytes = stream.AsyncRead(size)
        return bytes
    } 
    |> Async.RunSynchronously
    |> Encoding.UTF8.GetString
    |> XDocument.Parse

printfn "%A" <| getContract "localhost" 8090

open System
open Microsoft.FSharp.Quotations
open Microsoft.FSharp.Core
let xx = "Microsoft.FSharp.Core.FSharpFunc`2[System.Int32,System.Boolean]"
let vv = xx.TrimEnd(']').Split('[')
Type.GetType("System.IObservable`1[System.String]").GetGenericArguments()
Type.GetType(xx, true)

#r "FSharp.Core.dll"
typeof<Expr<_>>.ToString()
|> Type.GetType

open System
open System.Reactive
open System.Reactive.Linq
let x = Observable.Interval(TimeSpan.FromSeconds(1.0))
x.GetType().GetMethod("Subscribe")
let t = typedefof<int64>
let obsTy = typedefof<IObservable<_>> //.MakeGenericType [| t |]
let actTy = typedefof<Action<_>> //.MakeGenericType [| t |]
let m = typeof<ObservableExtensions>.GetMethods()
//        |> Array.find(fun t -> t.ToString() = "System.IDisposable Subscribe[T](System.IObservable`1[T], System.Action`1[T])")
        |> Array.map (fun m -> m.ToString())





open Microsoft.FSharp.Quotations
open Microsoft.FSharp.Quotations.DerivedPatterns
open Microsoft.FSharp.Quotations.ExprShape

let q = <@ fun (x : int) -> x = x @>

let rec substituteExpr expression =
    match expression with
    | ShapeVar var -> Expr.Var var
    | ShapeLambda (var, expr) ->
        let var2 = Var.Global(var.Name, typeof<float>) 
        Expr.Lambda (var2, substituteExpr expr)
    | ShapeCombination(shapeComboObject, exprList) ->
        RebuildShapeCombination(shapeComboObject, List.map substituteExpr exprList)

substituteExpr q

let itc = { 
    new ITypeNameConverter with 
        member __.ToDeserializedType (ti : TypeInfo) = 
                {
                    Name = ti.Name
                    AssemblyInfo = ti.AssemblyInfo    
                }
        member __.OfSerializedType ti = ti
    }
                 
            
open System
let o = Type.GetType("Microsoft.FSharp.Quotations.FSharpExpr`1, FSharp.Core, Version=4.3.1.0, Culture=neutral, PublicKeyToken=b03f5f7f11d50a3a")

"Microsoft.FSharp.Quotations.FSharpExpr`1[[Microsoft.FSharp.Core.FSharpFunc`2[[Record, TypeBuilderProvider.Debug, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null],[System.Boolean, mscorlib, Version=4.0.0.0, Culture=neutral, PublicKeyToken=b77a5c561934e089]], FSharp.Core, Version=4.3.1.0, Culture=neutral, PublicKeyToken=b03f5f7f11d50a3a]], FSharp.Core, Version=4.3.1.0, Culture=neutral, PublicKeyToken=b03f5f7f11d50a3a".Replace("[[", "<(").Replace("]]", ")>'").Replace("[","(").Replace("]",")")

open System

type Parser<'r> = Parser of (char list -> ('r*char list) list)

let parse (Parser p) = p
let (>>=) p f = Parser(fun cs ->
    List.concat [for (r,cs') in parse p cs -> parse (f r) cs'])

let (>>) p q = p >>= fun _ -> q
let mreturn r = Parser(fun cs -> [(r,cs)])

let lambda = Parser(fun _ -> [])

let item = Parser(fun cs ->
    match cs with [] -> [] | c::cs' -> [(c,cs')])

let sat cond =
  item >>= fun c -> if cond c then mreturn c else lambda

let char c = sat ((=)c)

let digit = sat (fun c ->
  (List.tryFind ((=)c) ['0'..'9']).IsSome)

let alpha = sat (fun c ->
    (List.tryFind ((=)c)(List.append
        ['a'..'z'] ['A'..'Z'])).IsSome)

let (<|>) p q = Parser(fun cs ->
  match parse p cs with
  | [] -> parse q cs
  | rs -> rs)

let (++) p q = Parser(fun cs ->
    List.append (parse p cs) (parse q cs))

let rec many0 p = many1 p <|> mreturn []
    and many1 p = p >>= fun r -> many0 p >>= fun rs -> mreturn (r::rs)

let id = many1 (sat (fun c -> not (c = '[' || c = ']')))

let rec symbol cs =
  match cs with
  | [] -> mreturn []
  | c::cs' -> char c >> symbol cs' >> mreturn cs

let sepBy p sep =
    p >>= fun r ->
    many0 (sep >> p) >>= fun rs ->
    mreturn (r::rs)

let (~&) (str:string) = str.ToCharArray() |> List.ofArray
let (~%) (chars:char list) = new String(Array.ofList chars)

type Type' = Type' of string * Type' list

let rec arg =
    ( char '[' >>
      id >>= fun name ->
      (char '[' >>
       sepBy arg (char ',') >>= fun args ->
       char ']' >>
       id >>= fun rest ->
       let fqn = %name + %rest
       char ']' >>
       mreturn(Type'(fqn, args)))
       <|> (char ']' >> mreturn(Type'(%name, []))))

let test =
    ( id >>= fun name -> 
      (char '[' >>
       sepBy arg (char ',') >>= fun args ->
       char ']' >> 
       id >>= fun rest ->
       let fqn = %name + %rest
       Type'(fqn, args)
       |> mreturn) <|> mreturn(Type'(%name, [])))

let uber = "Microsoft.FSharp.Quotations.FSharpExpr`1[[Microsoft.FSharp.Core.FSharpFunc`2[[Record, TypeBuilderProvider.Debug, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null],[System.Boolean, mscorlib, Version=4.0.0.0, Culture=neutral, PublicKeyToken=b77a5c561934e089]], FSharp.Core, Version=4.3.1.0, Culture=neutral, PublicKeyToken=b03f5f7f11d50a3a]], FSharp.Core, Version=4.3.1.0, Culture=neutral, PublicKeyToken=b03f5f7f11d50a3a"
let p = parse test &uber
