﻿namespace ObservableWebServiceFramework

open System
open System.Text
open System.Net.Sockets
open System.Threading
open Contract
open Network
open System.Net

module internal ClientHandler =
    
    let waitForCancellation (stream : NetworkStream) (socket : Socket) (cts : CancellationTokenSource) = async {      
        let! _, _ = readHeaderAsync stream
        cts.Cancel()
        close stream socket
        trace "Socket closed."
    } 
        
    let create service = 
        let contract = service |> extractServiceDescription |> string |> Encoding.UTF8.GetBytes
        let subscriptionHandler = Runtime.createSubscriptionHandler service
        fun socket -> async {
            let stream = new NetworkStream(socket, false) 
            let! methodIndex, size = readHeaderAsync stream
              
            match MessageType.fromInt methodIndex with
            | RequestContract -> 
                let contractBytes = createValueMessage RequestContract contract
                do! stream.AsyncWrite(contractBytes)
                trace "Contract request"
                close stream socket
                trace "Closed connection"
            
            | Subscribe -> 
                let! subscriptionBytes = stream.AsyncRead(size)
                let cts = new CancellationTokenSource()
                Async.Start(waitForCancellation stream socket cts)
                let subscription = subscriptionHandler subscriptionBytes stream
                trace "Started subscription"

            | _ -> 
                trace "Random error!"
                do! stream.AsyncWrite(createMessage OnError)
                close stream socket
        }

type ObservableWebService() =

    static member Create(service, hostname:string, ?port) =
        let ipAddress = Dns.GetHostEntry(hostname).AddressList.[0]
        ObservableWebService.Create(service, ipAddress, ?port = port)
 
    static member Create(service, ?ipAddress, ?port) =
        let clientHandler = ClientHandler.create service 
        let ipAddress = defaultArg ipAddress IPAddress.Any
        let port = defaultArg port 80
        let endpoint = IPEndPoint(ipAddress, port)
        let listener = new Socket(AddressFamily.InterNetwork, SocketType.Stream, ProtocolType.Tcp)
        listener.Bind(endpoint)
        listener.Listen(int SocketOptionName.MaxConnections)
        trace <| sprintf "Started listening on port %d" port
            
        let rec waitingForClient() = async {
            trace "Waiting for request ..."
            let! socket = listener.AsyncAccept()
            clientHandler socket |> Async.Start
            trace "Client connected ..."
            return! waitingForClient() }
            
        let cts = new CancellationTokenSource()
        Async.Start(waitingForClient(), cancellationToken = cts.Token)
        { new IDisposable with member __.Dispose() = cts.Cancel(); listener.Close() }

