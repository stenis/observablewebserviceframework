﻿module Main

open Microsoft.FSharp.Core.CompilerServices

[<TypeProviderAssembly("ObservableWebServiceFramework.DesignTime")>]
do ()

open System
open System.Reactive.Linq
open ObservableWebServiceFramework
open ObservableWebServiceFramework.Network

type Record = {
    Value : int64
    Key : string
}

type Service() =
    member __.Values with get () = Observable.Interval(TimeSpan.FromSeconds 1.0)
    member __.As with get () = Observable.Interval(TimeSpan.FromSeconds 1.0).Select(fun i -> { Key = "a"; Value = i})
    member __.Take (count : int) (obs : IObservable<Record>) = obs.Take(count)
    member __.Skip (count : int) (obs : IObservable<Record>) = obs.Skip(count)

let disposable = ObservableWebService.Create(Service(), port = 8090)
Console.ReadLine() |> ignore
disposable.Dispose()